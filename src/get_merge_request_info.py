"""
This module is for getting and operation information from Gitlab
"""
from datetime import datetime
import os
import gitlab
from loguru import logger


class GetMRsInfo:
    """
    Class operates with Gitlab API using python gitlab library
    """
    def __init__(self, gitlab_endpoint: str, private_token: str, project_name_with_namespace: str):
        kernel_git = gitlab.Gitlab(gitlab_endpoint, private_token=private_token)
        kernel_git.auth()
        self.project = kernel_git.projects.get(project_name_with_namespace)

    def get_number_of_merge_requests(self) -> int:
        """
        Gets total number of MRs
        :return: total number of MRs
        """
        merge_requests = self.project.mergerequests.list(iterator=True)
        logger.info(f'Total merge requests: {merge_requests.total}')
        return merge_requests.total

    def get_merge_requests_by_period(self, created_after, created_before):
        merge_requests = self.project.mergerequests.list(get_all=True, state='merged', order_by='updated_at', created_after=created_after, created_before=created_before)
        return {'first_mr_id': merge_requests[-1].iid, 'last_mr_id': merge_requests[0].iid, 'total_mrs': len(merge_requests)}

    def get_merge_request_state(self, mr_id: int) -> str:
        """
        Gets state of an MR
        :param mr_id: ID of MR
        :return: state of MR
        """
        try:
            merge_request = self.project.mergerequests.get(mr_id)
            return merge_request.state
        except gitlab.exceptions.GitlabGetError:
            logger.warning(f"Couldn't get an MR state. MR ID: {mr_id}")
            return ""

    def check_merge_request_state(self, mr_id: int, state: str) -> bool:
        """
        Checks if MR corresponds to the desired state

        :param mr_id: ID of MR
        :param state: There are 'opened', 'merged', 'closed' MRs
        :return: True or False
        """
        return self.get_merge_request_state(mr_id) == state

    def _get_merge_request_pipelines_list(self, mr_id: int) -> list:
        """
        Gets pipelines of the MR
        :param mr_id: ID of MR
        :return: List of pipeline IDs
        """
        try:
            merge_request = self.project.mergerequests.get(mr_id)
            return merge_request.pipelines.list(get_all=True)
        except gitlab.exceptions.GitlabGetError:
            logger.warning(f"Couldn't get MR pipelines. Looks like MR {mr_id} doesn't have any.")
            return []

    def get_list_jobs_of_pipeline(self, pipeline_id: int) -> list:
        """
        Gets list of jobs for the desired pipeline

        :param pipeline_id: ID of the pipeline
        :return: list of jobs
        """
        try:
            pipeline = self.project.pipelines.get(pipeline_id)
            bridges = pipeline.bridges.list()
            bridge_ids = []
            for bridge in bridges:
                # TODO check those magic keys :)
                bridge_ids.append(bridge.attributes['downstream_pipeline']['id'])
            return bridge_ids
        except gitlab.exceptions.GitlabGetError:
            logger.warning(f"Couldn't get list of jobs for the pipeline {pipeline_id}. Looks like it doesn't have any.")
            return []

    def _get_pipelines_attributes(self, mr_id: int) -> list:
        """
        Gets all pipeline attributes, i.e. all the info about the pipeline
        :param mr_id: ID of MR
        :return: pipeline attributes
        """
        pipelines_list = self._get_merge_request_pipelines_list(mr_id)
        attributes_list = []
        for pipeline in pipelines_list:
            attributes_list.append(pipeline.attributes)
        return attributes_list

    def get_all_sorted_merge_request_pipelines(self, mr_id: int) -> list:
        """
        Gets all MRs pipelines.
        Sort pipelines by `updated_at` value. The first pipeline in a list is the latest.
        :param mr_id: ID of MR
        :return: Sorted list of MRs pipelines
        """
        pipelines_attributes = self._get_pipelines_attributes(mr_id)
        return sorted(pipelines_attributes,
                      key=lambda t: datetime.strptime(t['updated_at'],
                                                      '%Y-%m-%dT%H:%M:%S.%f%z'))[::-1]

    def get_merge_request_pipelines_before_the_last_succeed(self, mr_id: int, pipeline_status: str = 'success') -> list:
        """
        Gets laa MR's pipelines before the lats pipeline that succeed
        :param mr_id:
        :param pipeline_status:
        :return: MR's pipelines before the lats pipeline that succeed
        """

        merge_request_pipelines_list = self.get_all_sorted_merge_request_pipelines(mr_id)

        for pipeline in range(len(merge_request_pipelines_list)):
            if merge_request_pipelines_list[pipeline]['status'] == pipeline_status:
                index = pipeline + 1
                break
        try:
            return merge_request_pipelines_list[index:]
        except UnboundLocalError:
            logger.warning(f"Couldn't get pipeline for the MR: {mr_id}. Looks like it doesn't have any.")
            return []


if __name__ == "__main__":
    gitlab_endpoint = 'https://gitlab.com'
    private_token = os.getenv('GITLAB_TOKEN')
    project_name_with_namespace = 'redhat/rhel/src/kernel/rhel-9'
    git = GetMRsInfo(gitlab_endpoint=gitlab_endpoint,
                     private_token=private_token,
                     project_name_with_namespace=project_name_with_namespace)

    git.get_merge_requests_by_dates(12, 22)
    # git.get_merge_request_pipelines_before_the_last_succeed(437, pipeline_status='success')
