"""
This module is for getting and operation information from AWS buckets
"""
import math
import re
import boto3


class AWSHelper:
    """
    This class is dealing with AWS api using boto3 library
    """
    def __init__(self, profile_name='arr_poweruser', client='s3', bucket='arr-cki-prod-datawarehouse-public'):
        self.bucket = bucket
        session = boto3.Session(profile_name=profile_name)
        self.s3 = session.client(client)

    def get_bucket_names(self, name: str = '-') -> list:
        """
        Gets bucket names by regex
        :param name: bucket name regex string
        :return: list of bucket names
        """
        buckets = self.s3.list_buckets()['Buckets']
        bucket_names = []
        for bucket in buckets:
            if re.search(name, bucket['Name'], re.I):
                bucket_names.append(bucket['Name'])

        return bucket_names

    def get_bucket_content(self, bucket: str, prefix: str = '') -> list:
        """
        Gets bucket content
        :param bucket: name of the bucket
        :param prefix: path to the files
        :return: bucket content
        """
        bucket = self.s3.list_objects_v2(Bucket=bucket, Prefix=prefix)
        try:
            bucket_content = bucket['Contents']
        except KeyError:
            return []
        return bucket_content

    @staticmethod
    def count_size(bucket_content: str) -> int:
        """
        Count size of the bucket content in bytes
        :param bucket_content: file patches in bucket
        :return: size in bytes
        """
        size = 0
        for content in bucket_content:
            size += content['Size']
        return size


def convert_size(size_bytes: int) -> str:
    """
    Converts bytes into human-readable format

    :param size_bytes: number of bytes
    :return: bytes into human-readable format
    """
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i_bytes = int(math.floor(math.log(size_bytes, 1024)))
    math_pow = math.pow(1024, i_bytes)
    s_bytes = round(size_bytes / math_pow, 2)
    return f"{s_bytes} {size_name[i_bytes]}"


def get_bucket_url_size(bucket_names_regex: str, pipeline_ids: list) -> dict:
    """
    For the provided bucket regex names gets content and size for each bucket.
    :param bucket_names_regex: bucket name regex string
    :param pipeline_ids: list of pipeline ids
    :return: bucket names, size, total size of the buckets
    """
    buckets = AWSHelper()
    cki_buckets = buckets.get_bucket_names(bucket_names_regex)
    total_size = 0
    res_buckets = []
    for bucket in cki_buckets:
        for pipeline_id in pipeline_ids:
            prefix = f'trusted-artifacts/{pipeline_id}/'
            content = buckets.get_bucket_content(bucket=bucket, prefix=prefix)
            size = AWSHelper.count_size(content)
            total_size += size

            url_size = {'url': f"https://s3.amazonaws.com/{bucket}/index.html?prefix={prefix}",
                        'size': convert_size(size),
                        'bucket_name': bucket}
            res_buckets.append(url_size)

    return {'buckets': res_buckets, 'total_size': total_size}


# def main(bucket_names_regex, pipeline_ids):
#
#     buckets = AWSHelper()
#     cki_buckets = buckets.get_bucket_names(bucket_names_regex)
#     total_size = 0
#     for bucket in cki_buckets:
#         for pipeline_id in pipeline_ids:
#             prefix = f'trusted-artifacts/{pipeline_id}/'
#             content = buckets.get_bucket_content(bucket=bucket, prefix=prefix)
#             size = AWSHelper.count_size(content)
#             total_size += size
#             print(f"      [{bucket}] [https://s3.amazonaws.com/{bucket}/index.html?prefix={prefix}] {convert_size(size)}")
#
#     print(f'    COULD BE REMOVED: {convert_size(total_size)}\n')


# def print_table_bucket_url_size(bucket_url_size):
#     from rich.console import Console
#     from rich.table import Table
#
#     table = Table(title="Bucket URL Size")
#
#     table.add_column("Bucket", style="cyan", no_wrap=True)
#     table.add_column("URL", style="magenta")
#     table.add_column("Size", justify="right", style="green")
#
#     for item in bucket_url_size['buckets']:
#         table.add_row(item['bucket_name'], item['url'], item['size'])
#
#     console = Console()
#     console.print(table)


if __name__ == "__main__":
    bucket_url_size = get_bucket_url_size(bucket_names_regex='arr-cki-prod-trusted-artifacts',
                                          pipeline_ids=[673852376, 673852371, 673852408, 673852386])
