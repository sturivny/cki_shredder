# AWS doc https://documentation.internal.cki-project.org/docs/background/storage/

import argparse
import os

from configparser import ConfigParser
from rich.console import Console
from rich import print as rprint
from rich.tree import Tree
from src.get_merge_request_info import GetMRsInfo
from src import aws_data


config = ConfigParser()
config.read('config.ini')

gitlab_endpoint = config.get('gitlab', 'gitlab_endpoint')
project_name_with_namespace = config.get('gitlab', 'project_name_with_namespace')
private_token = os.getenv('GITLAB_TOKEN')


def get_pipelines_in_merged_merge_requests_before_the_last_succeed(created_after: str, created_before: str):
    """
    Collects all together. This function basically pretty print to the output.

    :param created_after: Expected in ISO 8601 format (2022-10-29T08:00:00Z)
    :param created_before: Expected in ISO 8601 format (2022-11-28T08:00:00Z).
    :return: None
    """
    git = GetMRsInfo(gitlab_endpoint=gitlab_endpoint,
                     private_token=private_token,
                     project_name_with_namespace=project_name_with_namespace)
    merge_requests_by_period = git.get_merge_requests_by_period(created_after=created_after,
                                                                created_before=created_before)
    console = Console()
    console.log(f"\n[blue][bold]Merged MRs by period from "
                f"[bright_cyan]{created_after} [blue]to [bright_cyan]{created_before}", log_locals=False)
    console.log(merge_requests_by_period, log_locals=False)

    for mr_id in range(merge_requests_by_period['first_mr_id'], merge_requests_by_period['last_mr_id']+1):
        if git.check_merge_request_state(mr_id, state='merged'):
            mr_pipelines = git.get_merge_request_pipelines_before_the_last_succeed(mr_id)

            main_string = f"[green]MR URL: " \
                          f"[cyan]{gitlab_endpoint}/{project_name_with_namespace}" \
                          f"/-/merge_requests/{mr_id}/pipelines [white]Pipelines: {len(mr_pipelines)}"

            tree = Tree(main_string)
            total_bucket_size = 0
            for pipeline in mr_pipelines:
                jobs_list = git.get_list_jobs_of_pipeline(pipeline['id'])
                bucket_url_size = aws_data.get_bucket_url_size(bucket_names_regex='arr-cki-prod-trusted-artifacts',
                                                               pipeline_ids=jobs_list)
                for item in bucket_url_size['buckets']:
                    print_string = f"[green]{item['bucket_name']} [cyan]{item['url']} [magenta]{item['size']}"
                    tree.add(print_string)
                total_bucket_size += bucket_url_size['total_size']

            string_total_size = f"[red]TOTAL SIZE: [white]{aws_data.convert_size(total_bucket_size)}"
            if not total_bucket_size == 0:
                tree.add(string_total_size)

            print("")
            rprint(tree)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    created_after_help = "Returns merge requests created on or after the given time. " \
                         "Expected in ISO 8601 format (2022-10-29T08:00:00Z)."
    created_before_help = "Returns merge requests created on or before the given time. " \
                          "Expected in ISO 8601 format (2022-11-28T08:00:00Z)."

    required = parser.add_argument_group('Required named arguments')
    required.add_argument("-a", "--created_after", help=created_after_help, required=True)
    required.add_argument("-b", "--created_before", help=created_before_help, required=True)

    args = parser.parse_args()
    get_pipelines_in_merged_merge_requests_before_the_last_succeed(created_after=args.created_after,
                                                                   created_before=args.created_before)
