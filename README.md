## Get pipelines and jobs from Gitlab's MRs and 
Search for the created artifacts in the AWS buckets based on the pipelines and jobs from Gitlab's merge requests

The script searches for the merged Merge Requests between provided dates.
Once it finds an MR it looks for the executed jobs inside the MR and filters jobs by date.
In the end, it prints out all the jobs in the MR before the last `Succes` job.

Edit the `config.ini` file to change settings.

## How to use

Clone this project, create and activate virtual environment:
```commandline
$ virtualenv .venv
$ . .venv/bin/activate
```

Install `requirements.txt`:
```commandline
$ pip install -r requirements.txt
```

Export GITLAB_TOKEN as an environment variable:
```commandline
$ export GITLAB_TOKEN=<your_secret_token>
```

To run the script access to AWS accounts should be activated.
https://cyborg.pages.redhat.com/documentation/aws/access/

To run the script, `--created_after` and `--created_before` arguments should be provided.
```commandline
usage: main.py [-h] -a CREATED_AFTER -b CREATED_BEFORE

options:
  -h, --help            show this help message and exit

Required named arguments:
  -a CREATED_AFTER, --created_after CREATED_AFTER
                        Returns merge requests created on or after the given time. Expected in ISO 8601 format (2022-10-29T08:00:00Z).
  -b CREATED_BEFORE, --created_before CREATED_BEFORE
                        Returns merge requests created on or before the given time. Expected in ISO 8601 format (2022-11-28T08:00:00Z).
```

Example:
```commandline
$ python main.py --created_after  2022-10-29T08:00:00Z --created_before 2022-11-28T08:00:00Z
```
